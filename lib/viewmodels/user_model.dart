import 'package:innovitalia_app/enums/view_state.dart';
import 'package:innovitalia_app/locator.dart';
import 'package:innovitalia_app/models/user.dart';
import 'package:innovitalia_app/services/auth_service.dart';
import 'package:innovitalia_app/services/user_service.dart';
import 'package:innovitalia_app/viewmodels/base_model.dart';

class UserModel extends BaseModel {
  User user;

  UserModel() {
    user = null;
  }

  String getInitials() {
    return user.firstName.substring(0, 1) + user.lastName.substring(0, 1);
  }

  Future<void> loadUser() async {
    setState(ViewState.Busy);
    try {
      user = await locator<UserService>().fetchUser();
      setState(ViewState.Idle);
    } catch (e) {
      setState(ViewState.Idle);
      throw e;
    }
  }

  Future<void> updateUser(body) async {
    setState(ViewState.Busy);
    try {
      var temp = await locator<UserService>().patchUser(body);
      if (temp != null) user = temp;
      setState(ViewState.Idle);
    } catch (e) {
      setState(ViewState.Idle);
    }
  }

  logout() {
    locator<AuthService>().logOut();
  }
}
