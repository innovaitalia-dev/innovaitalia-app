import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:innovitalia_app/enums/message_type.dart';
import 'package:innovitalia_app/enums/view_state.dart';
import 'package:innovitalia_app/locator.dart';
import 'package:innovitalia_app/models/message.dart';
import 'package:innovitalia_app/services/chat_service.dart';
import 'package:innovitalia_app/viewmodels/base_model.dart';

class ChatModel extends BaseModel {
  List<Message> messages;
  Message nextQuestion;

  ChatModel() {
    messages = List<Message>();
  }

  Future<void> send(Message answer) async {
    if (answer.content != "") {
      messages.add(answer);
    }

    messages
        .add(Message(content: "Sta scrivendo...", type: MessageType.Waiting));

    setState(ViewState.Busy);

    List<Message> questions = await locator<ChatService>().sendMessage(answer);

    messages.removeLast();

    questions.forEach((item) {
      nextQuestion = item;
      if (nextQuestion.content != "") messages.add(nextQuestion);
    });

    setState(ViewState.Idle);
  }

  void clear() {
    messages = [];
  }
}
