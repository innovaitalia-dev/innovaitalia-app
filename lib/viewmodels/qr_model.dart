import 'package:flutter/material.dart';
import 'package:innovitalia_app/enums/view_state.dart';
import 'package:innovitalia_app/locator.dart';
import 'package:innovitalia_app/services/qr_service.dart';

import 'base_model.dart';

class QRModel extends BaseModel {
  String snapshot;

  QRModel() {
    generateQR();
  }

  Future<void> generateQR() async{
    setState(ViewState.Busy);
    try {
      this.snapshot = await locator<QRService>().getLastSnapshot();
      setState(ViewState.Idle);
    } catch (e){
      setState(ViewState.Idle);
      throw e;
    }

  }
}