import 'package:flutter/material.dart';
import 'package:innovitalia_app/enums/view_state.dart';

class BaseModel extends ChangeNotifier {
  ViewState state;

  void setState(ViewState viewState) {
    state = viewState;
    notifyListeners();
  }
}
