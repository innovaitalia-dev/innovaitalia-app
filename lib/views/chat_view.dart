import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:innovitalia_app/enums/message_type.dart';
import 'package:innovitalia_app/enums/view_state.dart';
import 'package:innovitalia_app/models/message.dart';
import 'package:innovitalia_app/viewmodels/chat_model.dart';
import 'package:innovitalia_app/widgets/custom_appbar.dart';
import 'package:innovitalia_app/widgets/custom_button.dart';
import 'package:innovitalia_app/widgets/custom_navbar.dart';
import 'package:provider/provider.dart';

class ChatView extends StatefulWidget {
  @override
  _ChatViewState createState() => _ChatViewState();
}

class _ChatViewState extends State<ChatView> {
  final TextEditingController controller = TextEditingController();

  @override
  void initState() {
    super.initState();
    if (Provider.of<ChatModel>(context, listen: false).messages.length == 0)
      Future.microtask(() => Provider.of<ChatModel>(context)
          .send(Message(content: "", type: MessageType.User)));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromRGBO(242, 244, 248, 1),
      appBar: CustomAppBar(
        title: "Stay Safe with AI",
      ),
      bottomNavigationBar: CustomNavBar(
        homeTap: () {
          Navigator.pop(context);
        },
        chatTap: () {},
        menuTap: () {
          Navigator.popAndPushNamed(context, "/menu");
        },
        background: Colors.white,
      ),
      body: Consumer<ChatModel>(
        builder: (context, chat, child) {
          return Stack(
            children: <Widget>[
              Positioned(
                bottom: 0,
                child: Container(
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(30),
                        topRight: Radius.circular(30)),
                  ),
                  height: MediaQuery.of(context).size.height - 250,
                  width: MediaQuery.of(context).size.width,
                  child: Column(
                    children: <Widget>[
                      Expanded(child: Chat(chat)),
                      /*ConstrainedBox(
                          child: Action(chat),
                          constraints: BoxConstraints(
                            maxHeight: 115,
                            minHeight: 75,
                          ),
                        ),*/
                    ],
                  ),
                ),
              ),
            ],
          );
        },
      ),
    );
  }
}

class Chat extends StatelessWidget {
  final ChatModel chat;
  Chat(this.chat);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Expanded(
          child: SingleChildScrollView(
            reverse: true,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: <Widget>[
                for (var index = 0; index < chat.messages.length; index++)
                  Padding(
                    padding: const EdgeInsets.symmetric(
                        vertical: 8.0, horizontal: 20.0),
                    child: Row(
                      mainAxisAlignment:
                          chat.messages[index].type == MessageType.User
                              ? MainAxisAlignment.end
                              : MainAxisAlignment.start,
                      children: <Widget>[
                        chat.messages[index].type == MessageType.Waiting
                            ? ChatMessage(
                                message: Message(
                                    content: "", type: MessageType.Waiting),
                                gif: AssetImage('assets/black_dots.gif'))
                            : ChatMessage(message: chat.messages[index]),
                      ],
                    ),
                  ),
                if (chat.state == ViewState.Idle &&
                    chat.nextQuestion != null &&
                    chat.nextQuestion.actions != null &&
                    chat.nextQuestion.actions.length != 0)
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: SizedBox(
                        width: MediaQuery.of(context).size.width * 0.85,
                        child: ChatChoices(chat: chat)),
                  ),
              ],
            ),
          ),
        ),
        SizedBox(
          height: 70,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: ChatField(chat: chat),
          ),
        ),
      ],
    );
  }
}

class ChatChoices extends StatefulWidget {
  const ChatChoices({
    Key key,
    @required this.chat,
  }) : super(key: key);

  final ChatModel chat;

  @override
  _ChatChoicesState createState() => _ChatChoicesState();
}

class _ChatChoicesState extends State<ChatChoices> {
  @override
  Widget build(BuildContext context) {
    return widget.chat.nextQuestion.actions.length > 2
        ? Wrap(
            runSpacing: 20,
            children: <Widget>[
              for (var i = 0; i < widget.chat.nextQuestion.actions.length; i++)
                ChatButton(
                    text: widget.chat.nextQuestion.actions[i]["label"],
                    onTap: () {
                      FocusScopeNode currentFocus = FocusScope.of(context);
                      if (!currentFocus.hasPrimaryFocus) {
                        currentFocus.unfocus();
                      }
                      widget.chat.send(Message(
                          content: widget.chat.nextQuestion.actions[i]["value"],
                          type: MessageType.User));
                    }),
            ],
          )
        : Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Flexible(
                fit: FlexFit.tight,
                flex: 1,
                child: ChatButton(
                    text: widget.chat.nextQuestion.actions[0]['label'],
                    onTap: () {
                      FocusScopeNode currentFocus = FocusScope.of(context);
                      if (!currentFocus.hasPrimaryFocus) {
                        currentFocus.unfocus();
                      }
                      widget.chat.send(Message(
                          content: widget.chat.nextQuestion.actions[0]['value'],
                          type: MessageType.User));
                    }),
              ),
              SizedBox(
                width: 20,
              ),
              Flexible(
                fit: FlexFit.tight,
                flex: 1,
                child: ChatButton(
                    text: widget.chat.nextQuestion.actions[1]['label'],
                    onTap: () {
                      FocusScopeNode currentFocus = FocusScope.of(context);
                      if (!currentFocus.hasPrimaryFocus) {
                        currentFocus.unfocus();
                      }
                      widget.chat.send(Message(
                          content: widget.chat.nextQuestion.actions[1]['value'],
                          type: MessageType.User));
                    }),
              ),
            ],
          );
  }
}

class ChatButton extends StatelessWidget {
  const ChatButton({@required this.text, @required this.onTap});

  final String text;
  final Function onTap;
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Material(
        elevation: 3,
        borderRadius: BorderRadius.all(Radius.circular(30)),
        child: Container(
          padding: EdgeInsets.all(10.0),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(30)),
              color: Colors.white),
          child: Center(
            child: Text(
              text,
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Theme.of(context).primaryColor,
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class ChatField extends StatefulWidget {
  const ChatField({
    @required this.chat,
  });

  final ChatModel chat;

  @override
  _ChatFieldState createState() => _ChatFieldState();
}

class _ChatFieldState extends State<ChatField> {
  final TextEditingController controller = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Flexible(
          flex: 10,
          child: TextFormField(
            onChanged: (value) {
              print(controller.text);
            },
            keyboardType: TextInputType.multiline,
            maxLines: null,
            controller: controller,
            decoration: InputDecoration(
              contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
              hintText: "Scrivi un messaggio",
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(32.0),
              ),
            ),
          ),
        ),
        Flexible(
          flex: 2,
          child: IconButton(
              icon: Icon(Icons.send),
              onPressed: widget.chat.state == ViewState.Idle
                  ? () {
                      FocusScopeNode currentFocus = FocusScope.of(context);
                      if (!currentFocus.hasPrimaryFocus) {
                        currentFocus.unfocus();
                      }
                      if (controller.text.trim().length > 0)
                        widget.chat.send(Message(
                            content: controller.text, type: MessageType.User));
                      controller.clear();
                    }
                  : null),
        )
      ],
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
    );
  }
}

class ChatMessage extends StatelessWidget {
  final AssetImage gif;
  final Message message;
  ChatMessage({this.message, this.gif});

  @override
  Widget build(BuildContext context) {
    return ConstrainedBox(
      constraints:
          BoxConstraints(maxWidth: MediaQuery.of(context).size.width * 0.8),
      child: Container(
        padding: const EdgeInsets.all(5.0),
        margin: EdgeInsets.only(top: 8),
        child: this.gif != null
            ? Image(
                image: this.gif,
                height: 15,
                width: 50,
              )
            : Html(
                shrinkToFit: true,
                data: message.content,
                defaultTextStyle: TextStyle(fontSize: 16),
              ),
        decoration: BoxDecoration(
          border: message.type == MessageType.User
              ? Border(
                  right: BorderSide(
                      color: Theme.of(context).accentColor, width: 3))
              : Border(
                  left: BorderSide(
                      color: Theme.of(context).primaryColor, width: 3)),
        ),
      ),
    );
  }
}