import 'package:flutter/material.dart';
import 'package:innovitalia_app/viewmodels/user_model.dart';
import 'package:innovitalia_app/widgets/custom_appbar.dart';
import 'package:provider/provider.dart';

class ProfileView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(
        title: "Modifica profilo",
        back: true,
      ),
      body: Consumer<UserModel>(builder: (context, user, child) {
        return SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(25.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                CustomSection(
                  title: 'Account',
                  field: [
                    {
                      "label": "email",
                      "name": "Email",
                      "value": user.user.email
                    }
                  ],
                  model: user,
                ),
                CustomSection(
                  title: 'Anagrafica',
                  field: [
                    {
                      "label": "firstName",
                      "name": "Nome",
                      "value": user.user.firstName
                    },
                    {
                      "label": "lastName",
                      "name": "Cognome",
                      "value": user.user.lastName
                    },
                    {
                      "label": "sex",
                      "name": "Sesso",
                      "value": user.user.sex == 0 ? "Maschio" : "Femmina"
                    },
                    {
                      "label": "birthDate",
                      "name": "Data di nascita",
                      "value": user.user.getBirthday()
                    },
                  ],
                  model: user,
                ),
                CustomSection(
                  title: 'Contatti',
                  field: [
                    {
                      "label": "phoneNumber",
                      "name": "Telefono",
                      "value": user.user.phoneNumber,
                      "controller":
                          TextEditingController(text: user.user.phoneNumber),
                    },
                    {
                      "label": "emergencyPhoneNumber",
                      "name": "Contatto di emergenza",
                      "value": user.user.emergencyNumber,
                      "controller": TextEditingController(
                          text: user.user.emergencyNumber),
                    },
                  ],
                  isModifiable: true,
                  model: user,
                ),
                CustomSection(
                  title: 'Indirizzo',
                  field: [
                    {
                      "label": "residencyCity",
                      "name": "Comune di residenza",
                      "value": user.user.residencyCity,
                      "controller":
                          TextEditingController(text: user.user.residencyCity),
                    },
                    {
                      "label": "residencyAddress",
                      "name": "Comune di residenza",
                      "value": user.user.residencyAddress,
                      "controller": TextEditingController(
                          text: user.user.residencyAddress),
                    },
                    {
                      "label": "domicileCity",
                      "name": "Comune di domicilio",
                      "value": user.user.domicileCity,
                      "controller":
                          TextEditingController(text: user.user.domicileCity),
                    },
                    {
                      "label": "domicileAddress",
                      "name": "Comune di residenza",
                      "value": user.user.domicileAddress,
                      "controller": TextEditingController(
                          text: user.user.domicileAddress),
                    }
                  ],
                  isModifiable: true,
                  model: user,
                ),
              ],
            ),
          ),
        );
      }),
    );
  }
}

class CustomSection extends StatefulWidget {
  final String title;
  final List<Map<String, dynamic>> field;
  final bool isModifiable;
  final UserModel model;

  const CustomSection(
      {Key key, this.title, this.field, this.isModifiable = false, this.model})
      : super(key: key);

  @override
  _CustomSectionState createState() => _CustomSectionState();
}

class _CustomSectionState extends State<CustomSection> {
  bool isEditing = false;

  void edit() {
    setState(() {
      this.isEditing = true;
    });
  }

  void save() async {
    Map<String, dynamic> body = Map<String, dynamic>();
    widget.field.forEach((item) {
      body[item['label']] = item['controller'].text;
    });
    await widget.model.updateUser(body);
    setState(() {
      isEditing = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          SectionTitle(
              title: this.widget.title,
              isModifiable: this.widget.isModifiable,
              status: isEditing,
              onEdit: edit,
              onSave: save),
          SectionContent(
              field: this.widget.field,
              isModifiable: this.widget.isModifiable,
              status: this.isEditing)
        ],
      ),
    );
  }
}

class SectionTitle extends StatefulWidget {
  final String title;
  final bool isModifiable;
  final bool status;
  final Function onEdit;
  final Function onSave;

  const SectionTitle(
      {Key key,
      this.title,
      this.isModifiable,
      this.status,
      this.onEdit,
      this.onSave})
      : super(key: key);

  @override
  _SectionTitleState createState() => _SectionTitleState();
}

class _SectionTitleState extends State<SectionTitle> {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        border: Border(
          left: BorderSide(color: Theme.of(context).primaryColor, width: 3),
        ),
      ),
      padding: const EdgeInsets.symmetric(horizontal: 12.0, vertical: 2),
      child: !widget.isModifiable
          ? Text(
              this.widget.title,
              style: TextStyle(
                  color: Theme.of(context).primaryColor, fontSize: 18),
            )
          : Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  this.widget.title,
                  style: TextStyle(
                      color: Theme.of(context).primaryColor, fontSize: 18),
                ),
                !widget.status
                    ? GestureDetector(
                        onTap: widget.onEdit,
                        child: Text(
                          "Modifica",
                          style: TextStyle(
                              color: Theme.of(context).accentColor,
                              fontSize: 16, fontWeight: FontWeight.bold),
                        ),
                      )
                    : GestureDetector(
                        onTap: widget.onSave,
                        child: Text(
                          "Salva",
                          style: TextStyle(
                              color: Theme.of(context).accentColor,
                              fontSize: 16, fontWeight: FontWeight.bold),
                        ),
                      )
              ],
            ),
    );
  }
}

class SectionContent extends StatefulWidget {
  final List<Map<String, dynamic>> field;
  final bool isModifiable;
  final bool status;

  const SectionContent(
      {Key key, this.field, this.isModifiable = false, this.status})
      : super(key: key);

  @override
  _SectionContentState createState() => _SectionContentState();
}

class _SectionContentState extends State<SectionContent> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(15.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          for (var field in widget.field)
            FieldInput(
              status: widget.status,
              isModifiable: widget.isModifiable,
              field: field,
            )
        ],
      ),
    );
  }
}

class FieldInput extends StatefulWidget {
  final bool status;
  final bool isModifiable;
  final Map<String, dynamic> field;

  const FieldInput({Key key, this.status, this.isModifiable, this.field})
      : super(key: key);

  @override
  _FieldInputState createState() => _FieldInputState();
}

class _FieldInputState extends State<FieldInput> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(bottom: 15),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: !widget.isModifiable
                ? const EdgeInsets.only(bottom: 8.0)
                : (widget.status
                    ? const EdgeInsets.all(0)
                    : const EdgeInsets.only(bottom: 8.0)),
            child: Text(
              widget.field['name'],
              style: TextStyle(color: Colors.black54, fontSize: 15),
            ),
          ),
          !widget.isModifiable
              ? Text(widget.field['value'])
              : (widget.status
                  ? TextFormField(
                      controller: widget.field['controller'],
                    )
                  : Text(widget.field['value']))
        ],
      ),
    );
  }
}
