import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:innovitalia_app/enums/view_state.dart';
import 'package:innovitalia_app/locator.dart';
import 'package:innovitalia_app/services/auth_service.dart';
import 'package:innovitalia_app/services/validation_service.dart';
import 'package:innovitalia_app/widgets/custom_button.dart';
import 'package:innovitalia_app/widgets/custom_appbar.dart';
import 'package:intl/intl.dart';


class RegistrationView extends StatefulWidget {
  @override
  _RegistrationView createState() => new _RegistrationView();
}

class _RegistrationView extends State<RegistrationView> {
//-----------------CREA ACCOUNT --------------------
  final emailController = TextEditingController();
  final passwordController = TextEditingController();
//-----------------ANAGRAFICA--------------------
  final nomeController = TextEditingController();
  final cognomeController = TextEditingController();
  final dataNascitaController = TextEditingController();
  final sessoController = TextEditingController();
  final comuneNascita = TextEditingController();
  final codicefiscale = TextEditingController();
//---------------------contatti-----------------------
  final telefonoController = TextEditingController();
  final telefonoEmergenzaController = TextEditingController();
//--------------------Residenza e domicilio-----------------
  final comuneResidenzaController = TextEditingController();
  final indirizzoResidenzaController = TextEditingController();
  final comuneDomicilioController = TextEditingController();
  final indirizzoDomicilioController = TextEditingController();
//-----------------------------------------------------------------
//-----------------------------------------------------------------
  final controlsBuilder = ControlsWidgetBuilder;
  ViewState viewState = ViewState.Idle;
  DateTime selectedDate = DateTime.now();
  bool checkBoxM = true;
  bool checkBoxF = false;
  int checkBoxValue = 0;
//--------------------------------------------------------
  final keys = [
    GlobalKey<FormState>(),
    GlobalKey<FormState>(),
    GlobalKey<FormState>(),
    GlobalKey<FormState>()
  ];

  void setViewState(ViewState viewState) {
    setState(() {
      this.viewState = viewState;
    });
  }

  bool isDifferent = false;

  Future _selectDate() async {
    var picked = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: DateTime(1920),
        lastDate: DateTime.now());

    if (picked != null) {
      selectedDate = picked;
      setState(() =>
          dataNascitaController.text = DateFormat("dd/MM/yyyy").format(picked));
    }
  }

  int currentStep = 0;
  bool complete = false;

  static var state = StepState.editing;

  next() {
    if (keys[currentStep].currentState.validate()) {
      if (currentStep + 1 != 4) {
        goTo(currentStep + 1);
      } else {
        print('here');
        register().then((value) {
          print('then');
          Navigator.popAndPushNamed(context, '/');
        }).catchError(
          (err) {
            print(err);
            Scaffold.of(context).showSnackBar(
              SnackBar(
                content: Text(
                  err.toString(),
                ),
              ),
            );
          },
        );
      }
    }
  }

  cancel() {
    if (currentStep > 0) {
      goTo(currentStep - 1);
    }
  }

  goTo(int step) {
    setState(() => currentStep = step);
  }


 bool passwordVisible = true;
  @override
  void initState() {
    setState(() {
      passwordVisible = false;
    });
    super.initState();
  }


  Widget build(BuildContext context) {
    return new Scaffold(
      backgroundColor: Colors.grey.shade200,
        appBar:CustomAppBar(
            title:'   Crea un nuovo account'
        ),
        body: Padding(
          padding: const EdgeInsets.fromLTRB(0,8.0,0,0),
          child: Container(
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(30),
                topRight: Radius.circular(30),
              ),
            ),

            child: Column(children: <Widget>[
              Expanded(
                child: Stepper(
                  controlsBuilder: (BuildContext context,
                      {VoidCallback onStepContinue, VoidCallback onStepCancel}) {
                    return Padding(
                      padding: const EdgeInsets.all(20.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Flexible(
                            flex: 2,
                            child: Container(
                              width: MediaQuery.of(context).size.width/2.5,
                              height: MediaQuery.of(context).size.width/7,
                                child: CustomButton(
                                   padding: EdgeInsets.all(3.0),
                                    name: 'Conferma',
                                    colorButton: Colors.white,
                                    colorName: Theme.of(context).primaryColor,
                                    action:onStepContinue,
                                )),
                          ),
                          Flexible(flex: 1, child: SizedBox(width: MediaQuery.of(context).size.width/55,),),
                          Flexible (
                            flex:2,
                            child: Container(
                                width: MediaQuery.of(context).size.width/2.5,
                                height: MediaQuery.of(context).size.width/7,
                                child: CustomButton(
                                    padding: EdgeInsets.all(3.0),
                                    name: 'Indietro',
                                    colorButton: Colors.white,
                                    colorName: Theme.of(context).primaryColor,
                                    action: onStepCancel)),
                          ),
                          //FlatButton(onPressed: onStepContinue, child: const Text('Continua'),),
                          //FlatButton(onPressed: onStepCancel, child: const Text('Indietro'),),
                        ],
                      ),
                    );
                  },
                  steps: [
                    Step(
                      title: const Text('Nuovo Account'),
                      isActive: currentStep == 0 ? true : false,
                      state: currentStep > 0 ? StepState.complete :  StepState.editing,
                      //state: state,
                      content: Form(
                        key: keys[0],
                        child: Column(
                          children: <Widget>[
                            CustomTextFormField(
                                hintText: 'Email',
                                inputAction: TextInputAction.go,
                                controller: emailController,
                                 myValidator: locator<ValidationService>().isMail
                             ),
                            CustomTextFormField(
                                hintText: 'Password',
                              myIcon: Icon(
                                passwordVisible ? Icons.visibility : Icons.visibility_off,
                                color: Theme.of(context).primaryColorDark,
                              ),
                              actionOnTap: (){
                                setState(() {passwordVisible = !passwordVisible;});
                                print('premuto tasto per oscurare il testo');
                              },
                                myObscureText:!passwordVisible,
                                inputAction: TextInputAction.go,
                                controller: passwordController,
                                myValidator: locator<ValidationService>().isPwd),
                          ],
                        ),
                      ),
                    ),
                    Step(
                      isActive: currentStep == 1 ? true : false,
                      state: currentStep > 1 ? StepState.complete : (currentStep == 1 ? StepState.editing : StepState.disabled),
                      title: const Text('Anagrafica'),
                      content: Form(
                        key: keys[1],
                        child: Column(
                          children: <Widget>[
                            CustomTextFormField(
                                hintText: 'Nome',
                                inputAction: TextInputAction.go,
                                controller: nomeController,
                              myValidator:locator<ValidationService>().isEmpty,),
                            CustomTextFormField(
                                hintText: 'Cognome',
                                inputAction: TextInputAction.go,
                                controller: cognomeController,
                              myValidator: locator<ValidationService>().isEmpty,),
                            //CustomTextFormField(hintText: 'Sesso', inputAction: TextInputAction.go, controller: sessoController),
                            InkWell(
                              onTap: () {
                                _selectDate();
                              },
                              child: IgnorePointer(
                                child: new TextFormField(
                                  decoration: new InputDecoration(
                                      labelText: "Data di nascita",
                                      //filled: false,
                                      suffixIcon: new Icon(Icons.calendar_today)),
                                  controller: dataNascitaController,
                                  validator: locator<ValidationService>().isEmpty,
                                ),
                              ),
                            ),
                            // CustomTextFormField(
                            //     hintText: 'Data di Nascita ',
                            //     inputAction: TextInputAction.go,
                            //     controller: dataNascitaController),
                            CustomTextFormField(
                                hintText: 'Codice fiscale  ',
                                inputAction: TextInputAction.go,
                                controller: codicefiscale,
                              myValidator:  locator<ValidationService>().isEmpty),
                            SizedBox(
                              width: 30,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Flexible(
                                  flex: 1,
                                  child: Text("Sesso",
                                      style: TextStyle (
                                          fontSize: MediaQuery.of(context).size.width/20
                                      )),
                                ),
                                Flexible(
                                  flex:1,
                                  child: Text("M",
                                      style: TextStyle (
                                          fontSize: MediaQuery.of(context).size.width/20
                                      )),
                                ),
                                Flexible(
                                  flex:1,
                                  child: Checkbox(
                                      value: checkBoxM,
                                      onChanged: (bool value){
                                        print("checkBoxM = ");
                                        print(value);
                                        setState(() {
                                          checkBoxM = value;
                                          if (checkBoxM){
                                            checkBoxValue = 0;
                                            checkBoxF = false;
                                          }
                                        });
                                      }
                                  ),
                                ),
                                Flexible(
                                  flex:1,
                                  child: Text("F",
                                      style: TextStyle (
                                          fontSize: MediaQuery.of(context).size.width/20
                                      )),
                                ),
                                Checkbox(
                                    value: checkBoxF,
                                    onChanged: (bool value) {
                                      print("checkBoxF = ");
                                      print(
                                          value);
                                      setState(
                                              () {
                                            checkBoxF = value;
                                            if (checkBoxM){
                                              checkBoxValue = 1;
                                              checkBoxM = false;
                                            }
                                          });
                                    }
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                    Step(
                      isActive: currentStep == 2 ? true : false,
                      state: currentStep > 2 ? StepState.complete : (currentStep == 2 ? StepState.editing : StepState.disabled),
                      title: const Text('Contatti'),
                      content: Form(
                        key: keys[2],
                        child: Column(
                          children: <Widget>[
                            CustomTextFormField(
                                hintText: 'Telefono',
                                inputAction: TextInputAction.go,
                                controller: telefonoController,
                               myValidator:  locator<ValidationService>().isEmpty),
                            CustomTextFormField(
                                hintText: 'Telefono in caso di emergenza',
                                inputAction: TextInputAction.go,
                                controller: telefonoEmergenzaController,
                              myValidator:  locator<ValidationService>().isEmpty),
                          ],
                        ),
                      ),
                    ),
                    Step(
                      isActive: currentStep == 3 ? true : false,
                      state: currentStep == 3 ? StepState.editing : StepState.disabled,
                      title: const Text('Indirizzi'),
                      content: Form(
                        key: keys[3],
                        child: Column(
                          children: <Widget>[
                            CustomTextFormField(
                                hintText: 'Comune di residenza',
                                inputAction: TextInputAction.go,
                                controller: comuneResidenzaController,
                              myValidator: locator<ValidationService>().isEmpty),
                            CustomTextFormField(
                                hintText: 'Indirizzo di residenza',
                                inputAction: TextInputAction.go,
                                controller: indirizzoResidenzaController,
                              myValidator: locator<ValidationService>().isEmpty),
                            Row(
                              children: <Widget>[
                                Text("Residenza diversa da domicilio",
                                  textScaleFactor: (MediaQuery.of(context).size.width/MediaQuery.of(context).size.height)*1.3,),
                                Checkbox(
                                    value: isDifferent,
                                    onChanged: (bool value){
                                      print(value);
                                      setState(() {
                                        isDifferent = value;
                                      });
                                    }
                                )
                              ],
                            ),
                                if(isDifferent)
                            CustomTextFormField(
                                hintText: 'Comune di domicilio',
                                inputAction: TextInputAction.go,
                                controller: comuneDomicilioController,
                              myValidator:  locator<ValidationService>().isEmpty),
                                if(isDifferent)
                            CustomTextFormField(
                                hintText: 'Indirizzo di domicilio',
                                inputAction: TextInputAction.go,
                                controller: indirizzoDomicilioController,
                              myValidator:  locator<ValidationService>().isEmpty),

                          ],
                        ),
                      ),
                    )
                  ],
                  currentStep: currentStep,
                  onStepContinue: next,
                  onStepTapped: (step) => goTo(step),
                  onStepCancel: cancel,
                ),
              ),
            ]),
          ),
        ));
  }

  Future<void> register() async {
    print({
        "firstName": nomeController.text,
        "lastName": cognomeController.text,
        "birthDate": dataNascitaController.text,
        "phoneNumber": telefonoController.text,
        "email": emailController.text,
        "password": passwordController.text,
        "sex": checkBoxValue,
        "emergencyPhoneNumber" : telefonoEmergenzaController.text,
        "personalNumber": codicefiscale.text,
        "domicileCity": comuneDomicilioController.text.isNotEmpty? comuneDomicilioController.text : comuneResidenzaController.text,
        "domicileAddress": indirizzoDomicilioController.text.isNotEmpty ? indirizzoDomicilioController.text : indirizzoResidenzaController.text,
        "residencyCity": comuneResidenzaController.text,
        "residencyAddress": indirizzoResidenzaController.text,
      });
    setViewState(ViewState.Busy);
    try {
      await locator<AuthService>().signUp({
        "firstName": nomeController.text,
        "lastName": cognomeController.text,
        "birthDate": dataNascitaController.text,
        "phoneNumber": telefonoController.text,
        "email": emailController.text,
        "password": passwordController.text,
        "sex": checkBoxValue,
        "emergencyPhoneNumber" : telefonoEmergenzaController.text,
        "personalNumber": codicefiscale.text,
        "domicileCity": comuneDomicilioController.text.isNotEmpty ? comuneDomicilioController.text : comuneResidenzaController.text,
        "domicileAddress": indirizzoDomicilioController.text.isNotEmpty ? indirizzoDomicilioController.text : indirizzoResidenzaController.text,
        "residencyCity": comuneResidenzaController.text,
        "residencyAddress": indirizzoResidenzaController.text,
      });
      setViewState(ViewState.Idle);
    } catch (err) {
      setViewState(ViewState.Idle);
      throw err;
    }
  }
}

//---------------------CustomTextFormField------------------------------------
class CustomTextFormField extends StatelessWidget {
  final String hintText;
  final FocusNode node;
  final FocusNode nextNode;
  final TextEditingController controller;
  final TextInputAction inputAction;
  final Function actionOnTap;
  final Function myValidator;
  final bool  myObscureText;
  final Icon myIcon;

  CustomTextFormField(
      {@required this.hintText,
        this.myObscureText = false,
        this.myIcon,
      this.node,
      this.inputAction,
      this.nextNode,
      this.controller,
      this.actionOnTap,
      this.myValidator}); // aposizionale

  @override
  Widget build(BuildContext context) {
    return TextFormField(
        onTap: actionOnTap,
        controller: controller,
        textInputAction: inputAction,
        obscureText: myObscureText,
        style: TextStyle(fontSize: MediaQuery.of(context).size.width/20),
        keyboardType: TextInputType.multiline,
        maxLines: 1,
        minLines: null,
        decoration: InputDecoration(labelText: hintText, suffixIcon: myIcon),
        validator: myValidator,
        );
  }
}


