import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:innovitalia_app/enums/view_state.dart';
import 'package:innovitalia_app/locator.dart';
import 'package:innovitalia_app/models/user.dart';
import 'package:innovitalia_app/services/auth_service.dart';
import 'package:innovitalia_app/services/sp_service.dart';
import 'package:innovitalia_app/viewmodels/user_model.dart';
import 'package:innovitalia_app/widgets/custom_appbar.dart';
import 'package:innovitalia_app/widgets/custom_botshortcut.dart';
import 'package:innovitalia_app/widgets/custom_companybanner.dart';
import 'package:innovitalia_app/widgets/custom_info.dart';
import 'package:innovitalia_app/widgets/custom_navbar.dart';
import 'package:innovitalia_app/widgets/custom_qrshortcut.dart';
import 'package:provider/provider.dart';

class HomeView extends StatefulWidget {
  @override
  _HomeViewState createState() => _HomeViewState();
}

class _HomeViewState extends State<HomeView>{
  User user;
  @override
  void initState() {
    super.initState();
    Future.microtask(() => Provider.of<UserModel>(context).loadUser());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromRGBO(242, 244, 248, 1),
      appBar: CustomAppBar(
        title: Provider.of<UserModel>(context).state == ViewState.Idle
            ? "Ciao " + Provider.of<UserModel>(context).user.firstName
            : "Ciao",
        back: false,
      ),
      bottomNavigationBar: CustomNavBar(
        homeTap: () {},
        chatTap: () {
          Navigator.pushNamed(context, '/chat');
        },
        menuTap: () {
          Navigator.pushNamed(context, '/menu');
        },
      ),
      body: Consumer<UserModel>(
        builder: (context, user, child) {
          return SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.fromLTRB(30, 10, 30, 30),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  // if(user.state == ViewState.Idle && user.user.companyId == null && locator<SpService>().getProperty("show_company_banner") == 'true')
                  // CustomCompanyBanner(closeTap: (){
                  //   setState(() {
                  //     locator<SpService>().setProperty("show_company_banner", false);
                  //   });
                  // } ),
                  // SizedBox(
                  //   height: 15,                
                  // ),
                  CustomBotShortcut(),
                  SizedBox(
                    height: 30,                
                  ),
                  CustomQRShortcut(),
                  SizedBox(
                    height: 30,                
                  ),
                  Text("Informazioni utili"),
                  SizedBox(
                    height: 20,                
                  ),
                  CustomInfo()
                ],
              ),
            ),
          );
        },
      ),
    );
  }

  void logout() {
    locator<AuthService>().logOut();
    Navigator.popAndPushNamed(context, '/');
  }
}
