import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:innovitalia_app/enums/view_state.dart';
import 'package:innovitalia_app/locator.dart';
import 'package:innovitalia_app/models/user.dart';
import 'package:innovitalia_app/services/auth_service.dart';
import 'package:innovitalia_app/services/validation_service.dart';
import 'package:innovitalia_app/widgets/custom_button.dart';


class LoginView extends StatefulWidget {
  @override
  _LoginViewState createState() => _LoginViewState();
}


class _LoginViewState extends State<LoginView> {
  final key = GlobalKey<FormState>();
  final passwordController = TextEditingController();
  final emailController = TextEditingController();
  ViewState viewState = ViewState.Idle;

  bool passwordVisible = false;
  @override
  void initState() {
    passwordVisible = false;
  }

  bool _autovalidate = false;
  void _error() {
    setState(() {
      _autovalidate = true;
    });
  }

  void setViewState(ViewState viewState) {
    setState(() {
      this.viewState = viewState;
    });
  }

  @override
  Widget build(BuildContext context) {
    FocusNode nodeEmail = FocusNode();
    FocusNode nodePassword = FocusNode();
    FocusNode unusedFocused = FocusNode();



    // TODO: implement build
    return SafeArea(
      child: Scaffold(
          body:  Stack(
          children: <Widget>[
        Container(
            decoration: BoxDecoration(
              //color: Colors.redAccent,
              color: Theme.of(context).primaryColor,
            ),
        ),
        Positioned(
            top: MediaQuery.of(context).size.height / 5,
            child: Container(
              height: MediaQuery.of(context).size.height*(4/5),
              decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: new BorderRadius.only(
              topLeft: const Radius.circular(60.0),
              topRight: const Radius.circular(60.0),
            )),
              child: SingleChildScrollView(
            scrollDirection: Axis.vertical,
            child: Center(
              child: Container(
                width: MediaQuery.of(context).size.width, // larghezza Tutto schermo
                height: MediaQuery.of(context).size.height, // altezza Tutto schermo
                //color: Colors.deepPurple.shade100,
                child: Padding(
                  padding: const EdgeInsets.all(45),
                  child: Form(
                    key: key,
                    child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Container(
                            alignment: Alignment.centerLeft,
                            //color: Colors.green,
                            child: Column(
                                crossAxisAlignment:
                                    CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text('Ciao',
                                      style: TextStyle(
                                        fontSize: 40,
                                      )),
                                  Text('Accedi con le tue credenziali',
                                      style: TextStyle(
                                          fontSize: 15, color: Colors.grey))
                                ]),
                          ),
                          //CircleAvatar(backgroundColor: Colors.transparent, radius: 60, backgroundImage: AssetImage('assets/logo_g.gif'),),
                          SizedBox(
                              height:
                                  MediaQuery.of(context).size.height / 30),
                          LoginField(
                              k0ork1: 0,
                              hintText: 'Email',
                              inputAction: TextInputAction.done,
                              node: nodeEmail,
                              nextNode: nodePassword,
                              controller: emailController,
                              validate:
                                  locator<ValidationService>().isMail),

                          SizedBox(
                              height:
                                  MediaQuery.of(context).size.height / 30),
                          LoginField(
                            k0ork1: 1,
                            hintText: 'Password',
                            myIcon: Icon(
                              passwordVisible
                                  ? Icons.visibility
                                  : Icons.visibility_off,
                              color: Theme.of(context).primaryColorDark,
                            ),
                            actionOnTap: () {
                              setState(() {
                                passwordVisible = !passwordVisible;
                              });
                            },
                            hide: !passwordVisible,
                            inputAction: TextInputAction.done,
                            node: nodePassword,
                            nextNode: unusedFocused,
                            controller: passwordController,
                            validate: locator<ValidationService>().isPwd,
                          ),
                          SizedBox(
                            height: MediaQuery.of(context).size.height / 15,
                          ),
                          SizedBox(
                            width: MediaQuery.of(context).size.width / 1.4,
                            child: Material(
                              elevation: 5.0,
                              borderRadius: BorderRadius.circular(30.0),
                              color: Colors.white,
                              child: MaterialButton(
                                height:
                                    MediaQuery.of(context).size.height / 15,
                                //padding: padding,
                                onPressed: () {
                                  print('Tasto Google premuto');
                                },
                                child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.center,
                                    children: <Widget>[
                                      Padding(
                                        padding: const EdgeInsets.all(10.0),
                                        child: Container(
                                          //padding: EdgeInsetsGeometry,
                                          child: CircleAvatar(
                                            backgroundColor:
                                                Colors.transparent,
                                            radius: 15,
                                            backgroundImage: AssetImage(
                                                'assets/Google__G__Logo.png'),
                                          ),
                                        ),
                                      ),
                                      Text('Google',
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                            fontSize: MediaQuery.of(context).size.width/20,
                                            color: Colors.black,
                                            //fontWeight: FontWeight.bold
                                          )),
                                    ]),
                              ),
                            ),
                          ),
                          SizedBox(
                            height: MediaQuery.of(context).size.height / 25,
                          ),
                          SizedBox(
                            width: MediaQuery.of(context).size.width / 1.4,
                            child: Builder(builder: (context) {
                              return CustomButton(
                                  name: viewState == ViewState.Idle
                                      ? 'Accedi'
                                      : 'Caricamento...',
                                  colorButton:
                                  viewState == ViewState.Idle ? 
                                      Theme.of(context).primaryColor : Colors.grey,
                                  colorName: viewState == ViewState.Idle ? Colors.white : Colors.black,
                                  action: viewState == ViewState.Idle ? () {
                                    if (key.currentState.validate()) {
                                      _login().then((value) {
                                        Navigator.popAndPushNamed(
                                            context, '/home',
                                            arguments: value.toJson());
                                      }).catchError((error) {
                                        print(error);
                                        Scaffold.of(context).showSnackBar(
                                            new SnackBar(
                                                content: new Text(
                                                    error.toString())));
                                      });
                                    } else {
                                      _error();
                                    }
                                  } : null);
                            }),
                          ),
                          SizedBox(
                            width: MediaQuery.of(context).size.width,
                            height: MediaQuery.of(context).size.height / 10,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Container(
                                  child: Text("Non hai ancora un account?",
                                      style: TextStyle(color: Colors.grey),
                                    textScaleFactor: (MediaQuery.of(context).size.width/MediaQuery.of(context).size.height)*1.3,
                                  ),
                                ),
                                Container(
                                  alignment: Alignment.centerRight,
                                  //color: Colors.green,
                                  child: FlatButton(
                                    child: Text(
                                      "Registrati",
                                      //style: TextStyle(color: Theme.of(context).primaryColor)
                                    ),
                                    onPressed: () {
                                      Navigator.pushNamed(
                                          context, '/registration');
                                      print('tasto registrazione premuto');
                                    },
                                    focusColor:
                                        Theme.of(context).primaryColor,
                                  ),
                                )
                              ],
                            ),
                          ),
                        ]),
                  ),
                ),
              ),
            )),
            ),
        ),
      ])),
    );
  }

  Future<User> _login() async {
    setViewState(ViewState.Busy);
    try {
      var user = await locator<AuthService>().login(
          {"email": emailController.text, "password": passwordController.text});
      setViewState(ViewState.Idle);
      return user;
    } catch (err) {
      setViewState(ViewState.Idle);
      throw err;
    }
  }
}

class LoginField extends StatelessWidget {
  final String hintText;
  final FocusNode node;
  final FocusNode nextNode;
  final TextEditingController controller;
  final TextInputAction inputAction;
  final Function actionOnTap;
  final bool hide;
  final bool autovalidate;
  final Function validate;
  final Icon myIcon;
  final _formKey = GlobalKey<FormState>();
  static final keys = [GlobalKey<FormState>(), GlobalKey<FormState>()];
  final int k0ork1;

  LoginField(
      {@required this.hintText,
      this.k0ork1,
      this.node,
      this.inputAction,
      this.nextNode,
      this.controller,
      this.autovalidate,
      this.validate,
      this.myIcon,
      this.actionOnTap,
      this.hide = false}); // aposizionale

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      key: keys[k0ork1],
      validator: validate,
      controller: controller,
      textInputAction: inputAction,
      obscureText: hide,
      style: TextStyle(fontSize: MediaQuery.of(context).size.width/25),
      keyboardType: TextInputType.emailAddress,
      maxLines: 1,
      minLines: null,
      decoration: InputDecoration(
        suffixIcon: myIcon != null ? IconButton(onPressed: actionOnTap, icon: myIcon) : null,
        contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        hintText: hintText,
        //border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))
      ),
    );
  }
}
