import 'package:flutter/material.dart';

class TipsView extends StatefulWidget {
  final List<Map<String, dynamic>> pageInfo = [
    {
      "imagePath": "assets/idea.png",
      "title": "Cosa fare",
      "description": "se si vive con un positivo al Coronavirus"
    },
    {
      "imagePath": "images/AdobeStock_322385607.jpeg",
      "title": "Indossa una mascherina chirurgica",
      "description":
          "Assicurati di averla posizionata correttamente sul viso (il naso e la bocca devono essere coperti). Mentre la indossi evita di toccare la mascherina; se la tocchi, lavati le mani."
    },
    {
      "imagePath": "images/AdobeStock_320698538.jpeg",
      "title": "Lavati spesso le mani",
      "description":
          "Le mani vanno accuratamente lavate con acqua e sapone o con una soluzione idroalcolica dopo ogni contatto con il malato o con il suo ambiente cirostante."
    },
    {
      "imagePath": "images/AdobeStock_326459797.jpeg",
      "title": "Mantieni le distanze di sicurezza",
      "description":
          "La persona con sospetta o confermata Covid-19 deve stare lontana dagli altri familiari, se possibile in una stanza singola ben ventilata. Non deve uscire né ricevere visite."
    },
    {
      "imagePath": "images/AdobeStock_333403469.jpeg",
      "title": "Superfici e igiene",
      "description":
          "Le superfici toccate frequentemente dalla persona malata devono essere pulite e disinfettate ogni giorno con prodotti a base di alcool e candeggina."
    },
    {
      "imagePath": "images/AdobeStock_266283631.jpeg",
      "title": "Raccomandazioni",
      "description":
          "Stoviglie, posate, ascuigamani e lenzuola devono essere dedicate esclusivamente alla persona malata. Devono essere lavate spesso con detersivo e acqua a 60-90 °C."
    },
    {
      "imagePath": "images/AdobeStock_307998095.jpeg",
      "title": "Raccomandazioni",
      "description":
          "Tutti i rifiuti domestici devono essere gettati all'interno del contenitore utilizzato per la raccolta indifferenziata, ponendoli prima dentro almeno due sacchetti resistenti chiusi."
    },
  ];

  @override
  _TipsViewState createState() => _TipsViewState();
}

class _TipsViewState extends State<TipsView> {
  int _current = 0;

  List<T> map<T>(List list, Function handler) {
    List<T> result = [];
    for (var i = 0; i < list.length; i++) {
      result.add(handler(i, list[i]));
    }
    return result;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(30),
              child: Row(
                children: <Widget>[
                  GestureDetector(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: Icon(
                      Icons.arrow_back_ios,
                      color: Theme.of(context).primaryColor,
                    ),
                  ),
                ],
              ),
            ),
            if (_current != 0)
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 30.0),
                child: Text(
                  "Cosa fare",
                  style: TextStyle(
                      fontSize: 26, color: Theme.of(context).primaryColor),
                ),
              ),
            if (_current != 0)
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 30.0),
                child: Text(
                  "se si vive con positivo al Coronavirus",
                  style: TextStyle(fontSize: 18),
                ),
              ),
            if (_current != 0)
              SizedBox(
                height: 40,
              ),
            Expanded(
              child: PageView(
                onPageChanged: (index) {
                  setState(() {
                    _current = index;
                  });
                },
                controller: PageController(
                  initialPage: 0,
                ),
                children: <Widget>[
                  for (var i = 0; i < widget.pageInfo.length; i++)
                    i == 0
                        ? FirstTip(
                            imagePath: widget.pageInfo[i]['imagePath'],
                            title: widget.pageInfo[i]['title'],
                            description: widget.pageInfo[i]['description'])
                        : SingleTip(tipInfo: widget.pageInfo, i: i),
                ],
              ),
            ),
            SizedBox(
              height: 40,
            ),
            Padding(
              padding: const EdgeInsets.all(30.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: map<Widget>(widget.pageInfo, (index, url) {
                  return Container(
                    width: 7.0,
                    height: 7.0,
                    margin:
                        EdgeInsets.symmetric(vertical: 10.0, horizontal: 2.0),
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color:
                          _current == index ? Colors.black87 : Colors.black26,
                    ),
                  );
                }),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class ScrollTip extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(30),
                    bottomLeft: Radius.circular(30)),
                gradient: LinearGradient(colors: [
                  Theme.of(context).primaryColor,
                  Theme.of(context).accentColor
                ], stops: [
                  0.0,
                  0.8
                ]),
              ),
              padding: const EdgeInsets.symmetric(horizontal: 22, vertical: 14),
              child: Text(
                'Scorri per saperne di più',
                style: TextStyle(color: Colors.white, fontSize: 16),
              ))
        ],
      ),
    );
  }
}

class FirstTip extends StatelessWidget {
  final String imagePath;
  final String title;
  final String description;

  const FirstTip({Key key, this.imagePath, this.title, this.description})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          SizedBox(
            height: 20,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 30.0),
            child: Hero(
              tag: "how",
              child: Container(
                width: 100,
                height: 100,
                padding: const EdgeInsets.all(8),
                child: Image.asset(this.imagePath),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 30),
            child: Text(this.title,
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 25,
                )),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 30),
            child: Text(
              this.description,
              style: TextStyle(fontSize: 22),
              textAlign: TextAlign.center,
            ),
          ),
          SizedBox(
            height: 20,
          ),
          ScrollTip(),
        ],
      ),
    );
  }
}

class SingleTip extends StatelessWidget {
  const SingleTip({
    Key key,
    @required this.tipInfo,
    @required this.i,
  }) : super(key: key);

  final List tipInfo;
  final int i;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 30.0),
      child: Column(
        children: <Widget>[
          Flexible(
            flex: 2,
            fit: FlexFit.tight,
            child: Container(
              margin: EdgeInsets.symmetric(horizontal: 10),
              decoration: BoxDecoration(
                color: Theme.of(context).primaryColor,
                borderRadius: BorderRadius.all(
                  Radius.circular(20),
                ),
              ),
              child: ClipRRect(
                  borderRadius: BorderRadius.circular(20.0),
                  child: Image.asset(
                    tipInfo[i]['imagePath'],
                    fit: BoxFit.cover,
                  )),
            ),
          ),
          Flexible(
              flex: 1,
              fit: FlexFit.tight,
              child: SingleInfo(
                title: tipInfo[i]['title'],
                description: tipInfo[i]['description'],
              ))
        ],
      ),
    );
  }
}

class SingleInfo extends StatelessWidget {
  final String title;
  final String description;

  const SingleInfo({Key key, this.title = "a", this.description = "a"})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: <Widget>[
          SizedBox(height: 30),
          Text(
            this.title,
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 16, fontWeight: FontWeight.w700),
          ),
          SizedBox(height: 8),
          Text(
            this.description,
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 14),
          ),
        ],
      ),
    );
  }
}
