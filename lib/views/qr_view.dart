import 'package:flutter/material.dart';
import 'package:innovitalia_app/enums/view_state.dart';
import 'package:innovitalia_app/viewmodels/qr_model.dart';
import 'package:innovitalia_app/viewmodels/user_model.dart';
import 'package:innovitalia_app/widgets/custom_appbar.dart';
import 'package:provider/provider.dart';
import 'package:qr_flutter/qr_flutter.dart';

class QRView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: CustomAppBar(
          title: "My QR Code",
          back: true,
        ),
        body: ChangeNotifierProvider(
          builder: (context) => QRModel(),
          child: Consumer<QRModel>(builder: (context, qr, child) {
            return Container(
              child: Consumer<UserModel>(builder: (context, user, child) {
                return qr.state == ViewState.Busy
                    ? CircularProgressIndicator()
                    : (qr.snapshot == ""
                        ? Text("Non hai ancora fatto una conversation")
                        : Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 50, vertical: 15),
                          child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: <Widget>[
                                Container(
                                  padding: const EdgeInsets.symmetric(horizontal: 20),
                                  child: Text(
                                      "Ciao ${user.user.firstName},\nscannerizza questo QR Code per certificare il tuo stato di salute", textAlign: TextAlign.center, style: TextStyle(fontSize: 18),),
                                ),
                                QrImage(
                                  data: user.user.id,
                                ),
                              ],
                            ),
                        ));
              }),
            );
          }),
        ));
  }
}
