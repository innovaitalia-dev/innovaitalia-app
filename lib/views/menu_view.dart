import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:innovitalia_app/viewmodels/chat_model.dart';
import 'package:innovitalia_app/viewmodels/user_model.dart';
import 'package:innovitalia_app/widgets/custom_navbar.dart';
import 'package:provider/provider.dart';
import 'package:share/share.dart';

class MenuView extends StatelessWidget {
  final List<Widget> items = [];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromRGBO(242, 244, 248, 1),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.fromLTRB(20.0, 50.0, 20.0, 20.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                CustomProfileBox(
                  height: 325,
                  background: Theme.of(context).primaryColor,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Container(
                        height: 100,
                        width: 100,
                        child: CircleAvatar(
                          backgroundColor: Colors.white,
                          child: Text(
                            Provider.of<UserModel>(context).getInitials(),
                            style: TextStyle(fontSize: 50),
                          ),
                        ),
                      ),
                      CustomGreet(
                          name: Provider.of<UserModel>(context).user.firstName),
                      GestureDetector(
                        onTap: () {
                          Navigator.pushNamed(context, '/profile');
                        },
                        child: Container(
                          decoration: BoxDecoration(
                            border: Border.all(color: Colors.white),
                            borderRadius: BorderRadius.all(
                              Radius.circular(30),
                            ),
                          ),
                          padding: const EdgeInsets.symmetric(
                              horizontal: 30, vertical: 12),
                          child: Text(
                            "Visualizza il tuo profilo",
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.w300),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      )
                    ],
                  ),
                ),
                SizedBox(
                  height: 30,
                ),
                CustomProfileBox(
                  background: Colors.white,
                  child: Column(
                    children: [
                      CustomListTile(
                        imagePath: 'assets/gradient_qr-code.png',
                        title: "Genera codice QR",
                        onTap: () {
                          Navigator.pushNamed(context, '/qr');
                        },
                      ),
                      Divider(),
                      Builder(builder: (context) {
                        return CustomListTile(
                            imagePath: 'assets/stethoscope.png',
                            title: "La tua cartella clinica",
                            onTap: () {
                              Scaffold.of(context).showSnackBar(
                                SnackBar(
                                  content: Text("Coming Soon!"),
                                ),
                              );
                            });
                      }),
                      Divider(),
                      CustomListTile(
                        imagePath: 'assets/question.png',
                        title: "Aiuto",
                        onTap: () {},
                      ),
                      Divider(),
                      CustomListTile(
                        imagePath: 'assets/feed.png',
                        title: "Dillo ad un amico",
                        onTap: () {
                          RenderBox box = context.findRenderObject();
                          Share.share("Prova sharing",
                              sharePositionOrigin:
                                  box.localToGlobal(Offset.zero) & box.size);
                        },
                      ),
                      Divider(),
                      CustomListTile(
                        imagePath: 'assets/launch.png',
                        title: "Esci",
                        go: false,
                        onTap: () {
                          Provider.of<ChatModel>(context).clear();
                          Provider.of<UserModel>(context).logout();
                          Navigator.pop(context);
                          Navigator.popAndPushNamed(context, "/");
                        },
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
      bottomNavigationBar: CustomNavBar(
        homeTap: () {
          Navigator.popAndPushNamed(context, '/home');
        },
        chatTap: () {
          Navigator.popAndPushNamed(context, '/chat');
        },
        menuTap: () {},
      ),
    );
  }
}

class CustomListTile extends StatelessWidget {
  final String imagePath;
  final String title;
  final bool go;
  final Function onTap;

  const CustomListTile(
      {Key key, this.imagePath, this.title, this.go = true, this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      contentPadding: EdgeInsets.fromLTRB(35, 5, 15, 5),
      leading: Container(
        width: 25,
        height: 25,
        child: Image(
          image: AssetImage(this.imagePath),
        ),
      ),
      title: Text(this.title),
      trailing: this.go ? Icon(Icons.arrow_forward_ios) : null,
      onTap: this.onTap,
    );
  }
}

class CustomGreet extends StatelessWidget {
  final String name;

  const CustomGreet({Key key, this.name}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Text(
          "Ciao",
          style: TextStyle(
              fontWeight: FontWeight.w300, fontSize: 24, color: Colors.white),
        ),
        Text(
          this.name,
          style: TextStyle(
              fontWeight: FontWeight.w600, fontSize: 24, color: Colors.white),
        )
      ],
    );
  }
}

class CustomProfileBox extends StatelessWidget {
  final Color background;
  final Widget child;
  final double height;

  const CustomProfileBox({this.background, this.child, this.height});
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: this.height,
      width: MediaQuery.of(context).size.width,
      child: Material(
        elevation: 2,
        borderRadius: BorderRadius.all(Radius.circular(20)),
        child: Container(
          decoration: BoxDecoration(
            color: this.background,
            borderRadius: BorderRadius.all(Radius.circular(20)),
          ),
          child: this.child,
        ),
      ),
    );
  }
}
