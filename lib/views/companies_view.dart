import 'package:flutter/material.dart';
import 'package:innovitalia_app/widgets/custom_appbar.dart';

class CompaniesView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(back: true, title: "Aziende",),
    );
  }
}