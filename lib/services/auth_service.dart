import 'dart:convert';
import 'dart:io';
import 'package:innovitalia_app/locator.dart';
import 'package:innovitalia_app/models/user.dart';
import 'package:innovitalia_app/services/http_service.dart';
import 'package:innovitalia_app/services/jwt_service.dart';
import 'package:innovitalia_app/services/sp_service.dart';
import 'package:http/http.dart' as http;

class AuthService extends HttpService{

  isLogged() {
    dynamic token = locator<SpService>().getJWT();
    if (token != null) {
      try {
        print(JwtService.isExpired(token));
        if (JwtService.isExpired(token)) {
          return false;
        } else {
          return true;
        }
      } catch (e) {
        print(e);
        return false;
      }
    } else {
      return false;
    }
  }

  Future<User> login(dynamic body) {
    return http.post(baseUrl + '/auth/signin',
        body: jsonEncode(body),
        headers: getHeaders()).then((valore) {
      var resp = jsonDecode(valore.body);
      var body = resp['body'];
      if (resp['code'] >= 200 && resp['code'] <= 399) {
        locator<SpService>().setJwt(body["jwt"]);
        return User.fromJson(body["user"]);
      } else if (valore.statusCode == 401) {
        throw ("Autenticazione non riuscita");
      } else {
        throw ("errore sconosciuto");
      }
    });
  }

  Future<void> signUp(Map<String, dynamic> user) {
    print('qui');
    return http.post(baseUrl + '/auth/signup',
        body: jsonEncode(user),
        headers: getHeaders()).then((res) {
      var resp = jsonDecode(res.body);
      if (res.statusCode <= 399 && resp['code'] >= 200 && resp['code'] <= 399) {
        return;
      } else {
        throw ("Qualcosa è andato storto");
      }
    });
  }

  logOut() {
    locator<SpService>().removeJwt();
  }
}
