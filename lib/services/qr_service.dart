import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:innovitalia_app/services/http_service.dart';

class QRService extends HttpService {
  Future<String> getLastSnapshot() {
    return http
        .get(baseUrl + "/user/snapshot", headers: getHeaders(auth: true))
        .then((value) {
      var resp = jsonDecode(value.body);
      var body = resp["body"];
      if (resp["code"] >= 200 && resp["code"] <= 399) {
        return jsonEncode(body["snapshot"]);
      } else {
        if (resp["code"] == 404)
          return "";
        else
          throw "Qualcosa è andato storto";
      }
    }).catchError((err) {
      throw err;
    });
  }
}
