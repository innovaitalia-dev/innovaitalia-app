import 'dart:convert';

import 'package:innovitalia_app/models/user.dart';
import 'package:http/http.dart' as http;
import 'package:innovitalia_app/services/http_service.dart';

class UserService extends HttpService {
  Future<User> fetchUser() {
    return http
        .get(baseUrl + "/user", headers: getHeaders(auth: true))
        .then((value) {
      var resp = jsonDecode(value.body);
      var body = resp["body"];
      if (resp["code"] >= 200 && resp["code"] <= 399) {
        return User.fromJson(body["user"]);
      } else {
        throw "Qualcosa è andato storto";
      }
    }).catchError((err) {
      throw err;
    });
  }

  Future<User> patchUser(body) {
    return http
        .patch(baseUrl + "/user",
            headers: getHeaders(auth: true), body: jsonEncode(body))
        .then((value) {
      var resp = jsonDecode(value.body);
      var body = resp["body"];
      if (resp['code'] >= 200 && resp['code'] <= 399) {
        if ((body as Map<dynamic, dynamic>).isEmpty)
          return null;
        else
          return User.fromJson(body['user']);
      } else {
        throw "Qualcosa è andato storto";
      }
    });
  }
}
