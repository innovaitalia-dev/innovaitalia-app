import 'dart:io';

import 'package:innovitalia_app/env.dart' as env;
import 'package:innovitalia_app/locator.dart';
import 'package:innovitalia_app/services/sp_service.dart';

class HttpService {
  String baseUrl = env.url + ":" + env.port.toString() + "/mobile";

  Map<String, String> getHeaders({auth = false}) {
    Map<String, String> headers = {
      HttpHeaders.contentTypeHeader : "application/json",
    };

    if (auth)
    headers.addAll({
      HttpHeaders.authorizationHeader : "Bearer " + locator<SpService>().getJWT()
    });

    return headers;
  }
}