import 'dart:convert';

import 'package:innovitalia_app/models/message.dart';
import 'package:innovitalia_app/services/http_service.dart';
import 'package:http/http.dart' as http;

class ChatService extends HttpService {
  int index = -1;
  List<List<Map<String, dynamic>>> messages = [
    [
      {"type": "text", "text": "Ciao, oggi come stai?"}
    ],
    [
      {"type": "text", "text": "Che ne dici di monitorare il tuo stato?"},
      {
        "type": "options",
        "text": "Iniziamo?",
        "options": [
          {"label": "Sì", "value": "yes"},
          {"label": "No", "value": "no"}
        ]
      },
    ],
    [
      {"type": "text", "text": "D'accordo!"},
      {
        "type": "options",
        "text": "Hai febbre?",
        "options": [
          {"label": "Sì", "value": "yes"},
          {"label": "No", "value": "no"}
        ]
      }
    ],
    [
      {
        "type": "options",
        "text": "Hai tosse?",
        "options": [
          {"label": "Sì", "value": "yes"},
          {"label": "No", "value": "no"}
        ]
      }
    ],
  ];

  Future<List<Message>> sendMessage(Message message) async {
    var results = await http.post(baseUrl + '/conv/message', headers: this.getHeaders(auth: true), body: jsonEncode(message.toMap()));
    List<Message> messages = (jsonDecode(results.body)["body"]["messages"] as List<dynamic>).map((item) => Message.fromJson(item)).toList();
    return messages;
    // Future.delayed(Duration(milliseconds: 1500), () {
    //   return messages[index].map((item) => Message.fromJson(item)).toList();
    // });
  }
}
