class ValidationService {
  String isEmpty(String value) {
    if (value.isEmpty)
      return 'Campo obbligatorio';
    else
      return null;
  }

  String isMail(String value) {
    if (value.isEmpty)
      return "Email obbligatoria";
    else {
      Pattern pattern =
          r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
      RegExp regex = new RegExp(pattern);
      if (!regex.hasMatch(value))
        return 'Inserisci una email valida';
      else
        return null;
    }
  }

  String isPwd(String value) {
    if (value.isEmpty)
      return "Password obbligatoria";
    else {
      if (value.length < 6)
        return "Password troppo corta";
      else
        return null;
    }
  }
}
