import 'package:shared_preferences/shared_preferences.dart';

class SpService {
  SharedPreferences _sp;
  
  init() async {
    this._sp = await SharedPreferences.getInstance();
    setProperty("show_company_banner", true);
  }

  void setProperty(String key, dynamic value){
    _sp.setString(key, value.toString());
  }

  String getProperty(String key){
    return _sp.getString(key);
  }

  String getJWT() {
    var jwt = _sp.get("jwt");
    return jwt;
  }

  setJwt(jwt) {
    _sp.setString("jwt", jwt);
  }

  removeJwt(){
    _sp.remove("jwt");
  }

}