import 'package:get_it/get_it.dart';
import 'package:innovitalia_app/services/auth_service.dart';
import 'package:innovitalia_app/services/chat_service.dart';
import 'package:innovitalia_app/services/qr_service.dart';
import 'package:innovitalia_app/services/sp_service.dart';
import 'package:innovitalia_app/services/user_service.dart';
import 'package:innovitalia_app/services/validation_service.dart';

GetIt locator = GetIt.instance;

void setupLocator() {
  locator.registerLazySingleton(() => SpService());
  locator.registerLazySingleton(() => AuthService());
  locator.registerLazySingleton(() => UserService());
  locator.registerLazySingleton(() => ChatService());
  locator.registerLazySingleton(() => QRService());
  locator.registerLazySingleton(() => ValidationService());
}
