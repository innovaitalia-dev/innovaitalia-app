import 'package:flutter/material.dart';

class CustomBotShortcut extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.pushNamed(context, '/chat');
      },
      child: Material(
        borderRadius: BorderRadius.all(Radius.circular(30)),
        elevation: 3,
        child: SizedBox(
          width: double.infinity,
          height: 320, // altezza totale?
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Flexible(
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(30),
                      topRight: Radius.circular(30),
                    ),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: Text(
                      "Stay Safe\nwith AI",
                      style: TextStyle(fontSize: 40),
                    ),
                  ),
                ),
                flex: 1,
                fit: FlexFit.tight,
              ),
              Flexible(
                child: Container(
                  width: double.infinity,
                  decoration: BoxDecoration(
                    gradient: LinearGradient(colors: [
                      Theme.of(context).primaryColor,
                      Theme.of(context).accentColor
                    ], stops: [
                      0.0,
                      0.8
                    ]),
                    borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(30),
                      bottomRight: Radius.circular(30),
                    ),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(18.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Flexible(
                          flex: 1,
                          child: Text(
                            "Chatta con X\ne contribuisci anche tu alla lotta contro il Coronavirus",
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 20,
                                fontWeight: FontWeight.w400),
                          ),
                        ),
                        Container(
                          child: Image(
                            image: AssetImage('assets/logo_text.png'),
                          ),
                          width: 100,
                          height: 30,
                        )
                      ],
                    ),
                  ),
                ),
                flex: 1,
                fit: FlexFit.tight,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
