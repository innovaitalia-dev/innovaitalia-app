import 'package:flutter/material.dart';

class CustomQRShortcut extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.pushNamed(context, '/qr');
      },
      child: SizedBox(
        width: double.infinity,
        height: 65,
        child: Row(
          children: <Widget>[
            Flexible(
              flex: 1,
              child: Container(
                padding: const EdgeInsets.all(10.0),
                margin: const EdgeInsets.only(right: 10),
                child: Image.asset('assets/qr-code.png'),
              ),
            ),
            Flexible(
              flex: 4,
              child: Material(
                borderRadius: BorderRadius.all(Radius.circular(10)),
                elevation: 2,
                child: Container(
                  padding: const EdgeInsets.all(12.0),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                      color: Colors.white),
                  child: Container(
                    padding:
                        const EdgeInsets.symmetric(horizontal: 10, vertical: 3),
                    decoration: BoxDecoration(
                      border: Border(
                        left: BorderSide(
                            color: Theme.of(context).primaryColor, width: 3),
                      ),
                    ),
                    child: Text(
                        "Genera il tuo QR Code e certifica il tuo stato di salute attuale"),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
