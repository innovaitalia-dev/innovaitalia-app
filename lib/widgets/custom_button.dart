

import 'package:flutter/material.dart';

class CustomButton extends StatelessWidget{
  final String name;
  final Color colorButton;
  final Color colorName;
  final Function action;
  final EdgeInsets padding;
  final Function validator;

  CustomButton(
      {@required this.name,
        this.colorName,
        this.colorButton,
        this.validator,
        this.action, this.padding = const EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0)}); // aposizionale


  @override
  Widget build(BuildContext context) {
    // TODO: implement build
   return Material(
     elevation: 5.0,
     borderRadius: BorderRadius.circular(30.0),
     color: colorButton,
     child: MaterialButton(
       minWidth: MediaQuery.of(context).size.width/15,
       height: MediaQuery.of(context).size.height/15,
       padding: padding,
       onPressed: action,
       child: Text(
           name,
           textAlign: TextAlign.center,
           style: TextStyle(
             fontSize: MediaQuery.of(context).size.width/25,
               color: colorName,
               //fontWeight: FontWeight.bold
           )),
     ),
   );
  }
}