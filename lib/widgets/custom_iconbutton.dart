import 'package:flutter/material.dart';

class CustomIconButton extends StatelessWidget {
  final Image icon;
  final String title;
  final Function onPressed;

  const CustomIconButton(
      {@required this.icon, @required this.title, @required this.onPressed});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: this.onPressed,
      child: Container(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Container(
                child: this.icon,
                width: 20,
                height: 20,
              ),
              Text(title, style: TextStyle(fontSize: 12),)
            ],
          ),
        ),
      ),
    );
  }
}
