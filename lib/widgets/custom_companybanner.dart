import 'package:flutter/material.dart';

class CustomCompanyBanner extends StatelessWidget {
  final Function closeTap;

  const CustomCompanyBanner({Key key, this.closeTap}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      elevation: 3,
      borderRadius: BorderRadius.all(Radius.circular(15)),
      child: Container(
        padding: EdgeInsets.fromLTRB(10, 15, 10, 0),
        child: Column(
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  "Seleziona l'azienda",
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                ),
                GestureDetector(
                  child: Icon(
                    Icons.close,
                    color: Colors.black38,
                  ),
                  onTap: closeTap,
                )
              ],
            ), //head
            Container(
              margin: EdgeInsets.only(top: 10),
              child: Text(
                "Non hai ancora selezionato la tua azienda. Controlla se è tra le nostre partner!",
              ),
            ), //body
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                FlatButton(
                  onPressed: () {
                    Navigator.pushNamed(context, '/companies');
                  },
                  child: Text("VAI"),
                  textColor: Theme.of(context).primaryColor,
                )
              ],
            ) //action
          ],
        ),
      ),
    );
  }
}
