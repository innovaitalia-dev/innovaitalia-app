import 'package:flutter/material.dart';
import 'package:innovitalia_app/widgets/custom_iconbutton.dart';

class CustomNavBar extends StatefulWidget {
  final Function homeTap;
  final Function chatTap;
  final Function menuTap;
  final Color background;

  const CustomNavBar(
      {this.homeTap,
      this.chatTap,
      this.menuTap,
      this.background = Colors.transparent});

  @override
  _CustomNavBarState createState() => _CustomNavBarState();
}

class _CustomNavBarState extends State<CustomNavBar> {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 80,
      child: Container(
        color: widget.background,
        child: Material(
          elevation: 20,
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(30), topRight: Radius.circular(30)),
          child: Container(
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(30),
                topRight: Radius.circular(30),
              ),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                CustomIconButton(
                  icon: Image(image: AssetImage("assets/spaceship.png")),
                  title: "Home",
                  onPressed: widget.homeTap,
                ),
                CustomIconButton(
                  icon: Image(image: AssetImage("assets/dialogue.png")),
                  title: "Chat",
                  onPressed: widget.chatTap,
                ),
                CustomIconButton(
                  icon: Image(image: AssetImage("assets/advocate.png")),
                  title: "Profilo",
                  onPressed: widget.menuTap,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
