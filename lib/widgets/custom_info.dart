import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class CustomInfo extends StatefulWidget {
  @override
  _CustomInfoState createState() => _CustomInfoState();
}

class _CustomInfoState extends State<CustomInfo> {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      child: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: Padding(
          padding: const EdgeInsets.only(bottom: 10),
          child: Wrap(
            direction: Axis.horizontal,
            spacing: 20,
            alignment: WrapAlignment.spaceBetween,
            children: <Widget>[
              SingleInfo(
                imagePath: 'assets/doctor.png',
                title: "Numeri utili",
                description: "Emergenza Coronavirus",
                onTap: () async {
                  var url =
                      'http://www.salute.gov.it/portale/nuovocoronavirus/menuAChiRivolgersiNuovoCoronavirus.jsp?lingua=italiano&area=nuovoCoronavirus&menu=aChiRivorgersi';
                  if (await canLaunch(url)) {
                    await launch(
                      url,
                      forceSafariVC: false,
                      forceWebView: false,
                    );
                  } else {
                    throw 'Could not launch $url';
                  }
                },
              ),
              SingleInfo(
                imagePath: 'assets/feedback.png',
                title: "Aggiornamenti",
                description: "Coronavirus dal Ministero della\nSalute",
                onTap: () async {
                  var url =
                      'http://www.salute.gov.it/nuovocoronavirus?gclid=Cj0KCQjwudb3BRC9ARIsAEa-vUsSlD514M4U__ijkciUrz-1pV4UHPJS2WIOlE07AhvuEDMksLxZFRUaAhGuEALw_wcB';
                  if (await canLaunch(url)) {
                    await launch(
                      url,
                      forceSafariVC: false,
                      forceWebView: false,
                    );
                  } else {
                    throw 'Could not launch $url';
                  }
                }
              ),
              SingleInfo(
                imagePath: 'assets/idea.png',
                title: "Cosa fare",
                description: "se si vive con un positivo al Coronavirus",
                onTap: () {
                  Navigator.pushNamed(context, '/tips');
                },
                animate: true,
              )
            ],
          ),
        ),
      ),
    );
  }
}

class SingleInfo extends StatelessWidget {
  final String imagePath;
  final String title;
  final String description;
  final Function onTap;
  final bool animate;

  const SingleInfo(
      {Key key,
      this.imagePath,
      this.title,
      this.description,
      this.onTap,
      this.animate = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: SizedBox(
        height: 150,
        width: 150,
        child: Material(
          borderRadius: BorderRadius.all(Radius.circular(10)),
          color: Colors.white,
          elevation: 3,
          child: Padding(
            padding: const EdgeInsets.all(12.0),
            child: animate
                ? Column(
                    //crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Hero(
                        tag: 'how',
                        child: Container(
                          width: 50,
                          height: 50,
                          padding: const EdgeInsets.all(8),
                          child: Image.asset(this.imagePath),
                        ),
                      ),
                      Text(
                        this.title,
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 15,
                        ),
                      ),
                      Text(
                        this.description,
                        style: TextStyle(fontSize: 13),
                        textAlign: TextAlign.center,
                      ),
                    ],
                  )
                : Column(
                    //crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Container(
                        width: 50,
                        height: 50,
                        padding: const EdgeInsets.all(8),
                        child: Image.asset(this.imagePath),
                      ),
                      Text(
                        this.title,
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 15,
                        ),
                      ),
                      Text(
                        this.description,
                        style: TextStyle(fontSize: 13),
                        textAlign: TextAlign.center,
                      ),
                    ],
                  ),
          ),
        ),
      ),
    );
  }
}
