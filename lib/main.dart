import 'package:flutter/material.dart';
import 'package:innovitalia_app/locator.dart';
import 'package:innovitalia_app/services/auth_service.dart';
import 'package:innovitalia_app/services/sp_service.dart';
import 'package:innovitalia_app/viewmodels/chat_model.dart';
import 'package:innovitalia_app/viewmodels/user_model.dart';
import 'package:innovitalia_app/views/chat_view.dart';
import 'package:innovitalia_app/views/companies_view.dart';
import 'package:innovitalia_app/views/home_view.dart';
import 'package:innovitalia_app/views/login_view.dart';
import 'package:innovitalia_app/views/menu_view.dart';
import 'package:innovitalia_app/views/profile_view.dart';
import 'package:innovitalia_app/views/qr_view.dart';
import 'package:innovitalia_app/views/registration_view.dart';
import 'package:innovitalia_app/views/tips_view.dart';
import 'package:provider/provider.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  setupLocator();
  await locator<SpService>().init();
  runApp(MultiProvider(
    providers: [
      ChangeNotifierProvider(builder: (context) => UserModel()),
      ChangeNotifierProvider(builder: (context) => ChatModel())
    ],
    child: MyApp(),
  ));
}

class MyApp extends StatelessWidget {
  final Map<int, Color> primary = {
    50:Color.fromRGBO(15, 99, 254, .1),
    100:Color.fromRGBO(15, 99, 254, .2),
    200:Color.fromRGBO(15, 99, 254, .3),
    300:Color.fromRGBO(15, 99, 254, .4),
    400:Color.fromRGBO(15, 99, 254, .5),
    500:Color.fromRGBO(15, 99, 254, .6),
    600:Color.fromRGBO(15, 99, 254, .7),
    700:Color.fromRGBO(15, 99, 254, .8),
    800:Color.fromRGBO(15, 99, 254, .9),
    900:Color.fromRGBO(15, 99, 254, 1),
  };

  final Map<int, Color> accent = {
    50:Color.fromRGBO(165, 110, 255, .1),
    100:Color.fromRGBO(165, 110, 255, .2),
    200:Color.fromRGBO(165, 110, 255, .3),
    300:Color.fromRGBO(165, 110, 255, .4),
    400:Color.fromRGBO(165, 110, 255, .5),
    500:Color.fromRGBO(165, 110, 255, .6),
    600:Color.fromRGBO(165, 110, 255, .7),
    700:Color.fromRGBO(165, 110, 255, .8),
    800:Color.fromRGBO(165, 110, 255, .9),
    900:Color.fromRGBO(165, 110, 255, 1),
  };
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Health Bot',
      routes: {
        '/': (context) => locator<AuthService>().isLogged() == true
            ? HomeView()
            : LoginView(),
        '/registration': (_) => RegistrationView(),
        '/home': (_) => HomeView(),
        '/chat' : (_) => ChatView(),
        '/menu': (_) => MenuView(),
        '/profile': (_) => ProfileView(),
        '/qr' : (_) => QRView(),
        '/tips' : (_) => TipsView(),
        '/companies': (_) => CompaniesView(),
      },
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: MaterialColor(0xFF0F62FE, primary),
        accentColor: MaterialColor(0xFFA56EFF, accent),
      ),
      //home: LoginView(),
    );
  }
}
