class User {
  String id;
  String firstName;
  String lastName;
  DateTime birthDate;
  String phoneNumber;
  String emergencyNumber;
  String email;
  int sex;
  String personalNumber;
  String domicileCity;
  String domicileAddress;
  String residencyCity;
  String residencyAddress;
  String companyId;

  User(
    this.id,
      this.firstName,
      this.lastName,
      birthDate,
      this.phoneNumber,
      this.emergencyNumber,
      this.email,
      this.sex,
      this.personalNumber,
      this.domicileCity,
      this.domicileAddress,
      this.residencyCity,
      this.residencyAddress,
      this.companyId) {
    this.birthDate = DateTime.parse(birthDate);
  }

  factory User.fromJson(Map<String, dynamic> json) {
    return User(
      json['_id'],
        json['firstName'],
        json['lastName'],
        json['birthDate'],
        json['phoneNumber'],
        json['emergencyPhoneNumber'],
        json['email'],
        json['sex'],
        json['personalNumber'],
        json['domicileCity'],
        json['domicileAddress'],
        json['residencyCity'],
        json['residencyAddress'],
        json['companyId'] == null ? json['companyId'] : '');
  }

  Map<String, dynamic> toJson() {
    return {
      'firstName': this.firstName,
      'lastName': this.lastName,
      'birthDate': this.birthDate.toString(),
      'phoneNumber': this.phoneNumber,
      'emergencyPhoneNumber' : this.emergencyNumber,
      'email': this.email,
      'sex': this.sex,
      'personalNumber': this.personalNumber,
      'domicileCity': this.domicileCity,
      'domicileAddress': this.domicileAddress,
      'residencyCity': this.residencyCity,
      'residencyAddress': this.residencyAddress
    };
  }

  String getBirthday(){
    return '${this.birthDate.day}/${this.birthDate.month}/${this.birthDate.year}';
  }
}
