import 'package:flutter/widgets.dart';
import 'package:innovitalia_app/enums/message_type.dart';

class Message {
  String content;
  MessageType type;
  List<dynamic> actions;
  
  Message({@required this.content, @required this.type, this.actions});

  Message.fromJson(json){
    this.content = json['text'];
    this.type = MessageType.Bot;
    this.actions = json['options'];
  }

  Map<String, String> toMap(){
    return {
      "message" : this.content
    };
  }

}